// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDSTypes.h"
#include "TDS_CPP_SB.h"
#include "TDS_CPP_SB/TDS_CPP_SBIGameActor.h"


void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTDS_CPP_SB_StrateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UTDS_CPP_SB_StrateEffect* myEffect = Cast<UTDS_CPP_SB_StrateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UTDS_CPP_SB_StrateEffect*> CurrentEffects;
						ITDS_CPP_SBIGameActor* myInterface = Cast<ITDS_CPP_SBIGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}

					}
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{

						UTDS_CPP_SB_StrateEffect* NewEffect = NewObject<UTDS_CPP_SB_StrateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor);
						}
					}

				}
				i++;
			}
		}

	}
}