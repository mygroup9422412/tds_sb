// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_CPP_SB.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_CPP_SB, "TDS_CPP_SB" );

DEFINE_LOG_CATEGORY(LogTDS_CPP_SB)
 