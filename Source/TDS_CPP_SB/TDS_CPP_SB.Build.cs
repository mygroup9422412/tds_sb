// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_CPP_SB : ModuleRules
{
	public TDS_CPP_SB(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "HeadMountedDisplay", "CoreUObject", "Engine", "InputCore", "NavigationSystem", "AIModule", "Niagara", "EnhancedInput", "PhysicsCore", "Slate", "GameplayAbilities" });
        PublicDependencyModuleNames.Add("GameplayTasks");
    }

}
