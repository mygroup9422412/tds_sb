// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_CPP_SB_StrateEffect.h"
#include "TDS_CPP_SB/TPS_HealthComponent.h"
#include "TDS_CPP_SB/TDS_CPP_SBIGameActor.h"
#include "Kismet/GameplayStatics.h"

bool UTDS_CPP_SB_StrateEffect::InitObject(AActor* Actor)
{

	myActor = Actor;

	ITDS_CPP_SBIGameActor* myInterface = Cast<ITDS_CPP_SBIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UTDS_CPP_SB_StrateEffect::DestroyObject()
{
	ITDS_CPP_SBIGameActor* myInterface = Cast<ITDS_CPP_SBIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDS_CPP_SB_StrateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTDS_CPP_SB_StrateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_CPP_SB_StrateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPS_HealthComponent* myHealthComp = Cast<UTPS_HealthComponent>(myActor->GetComponentByClass(UTPS_HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTDS_CPP_SB_StrateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_CPP_SB_StrateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_CPP_SB_StrateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}

	return true;
}

void UTDS_CPP_SB_StrateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTDS_CPP_SB_StrateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
		UTPS_HealthComponent* myHealthComp = Cast<UTPS_HealthComponent>(myActor->GetComponentByClass(UTPS_HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}
