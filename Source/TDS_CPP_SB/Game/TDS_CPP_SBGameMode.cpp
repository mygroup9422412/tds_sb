// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_CPP_SBGameMode.h"
#include "TDS_CPP_SBPlayerController.h"
#include "TDS_CPP_SB/Character/TDS_CPP_SBCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDS_CPP_SBGameMode::ATDS_CPP_SBGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_CPP_SBPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}


}

void ATDS_CPP_SBGameMode::PlayerCharacterDead()
{
}
