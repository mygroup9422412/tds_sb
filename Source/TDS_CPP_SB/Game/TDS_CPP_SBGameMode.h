// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_CPP_SBGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_CPP_SBGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_CPP_SBGameMode();

	void PlayerCharacterDead();
};



