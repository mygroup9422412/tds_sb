// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_CPP_SBEnvironmentStructure.h"

// Sets default values
ATDS_CPP_SBEnvironmentStructure::ATDS_CPP_SBEnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDS_CPP_SBEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_CPP_SBEnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATDS_CPP_SBEnvironmentStructure::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTDS_CPP_SB_StrateEffect*> ATDS_CPP_SBEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_CPP_SBEnvironmentStructure::RemoveEffect(UTDS_CPP_SB_StrateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDS_CPP_SBEnvironmentStructure::AddEffect(UTDS_CPP_SB_StrateEffect* newEffect)
{
	Effects.Add(newEffect);
}

