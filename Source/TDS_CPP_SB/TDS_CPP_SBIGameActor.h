// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS_CPP_SB_StrateEffect.h"
#include "TDS_CPP_SBIGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_CPP_SBIGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_CPP_SB_API ITDS_CPP_SBIGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfuceType();

	virtual TArray<UTDS_CPP_SB_StrateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_CPP_SB_StrateEffect* RemoveEffect);
	virtual void AddEffect(UTDS_CPP_SB_StrateEffect* newEffect);
};
