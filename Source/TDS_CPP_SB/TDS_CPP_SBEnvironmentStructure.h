// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_CPP_SBIGameActor.h"
#include "TDS_CPP_SB/TDS_CPP_SB_StrateEffect.h"
#include "TDS_CPP_SBEnvironmentStructure.generated.h"

UCLASS()
class TDS_CPP_SB_API ATDS_CPP_SBEnvironmentStructure : public AActor, public ITDS_CPP_SBIGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_CPP_SBEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfuceType() override;

	TArray<UTDS_CPP_SB_StrateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDS_CPP_SB_StrateEffect* RemoveEffect)override;
	void AddEffect(UTDS_CPP_SB_StrateEffect* newEffect)override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UTDS_CPP_SB_StrateEffect*> Effects;
};
