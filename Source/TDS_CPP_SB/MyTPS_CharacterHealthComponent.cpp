// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTPS_CharacterHealthComponent.h"

void UMyTPS_CharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0f)
		{
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UMyTPS_CharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UMyTPS_CharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UMyTPS_CharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
		if(NiagaraCrushComponent && Shield == 0.0f)
		{ 
			AActor* TargetActor = GetWorld()->GetFirstPlayerController()->GetPawn();
		FVector SpawnNiagaraLocation;
		SpawnNiagaraLocation = TargetActor->GetActorLocation();
		NiagaraCrushComponent->SetWorldScale3D(FVector(0.1f, 0.1f, 0.1f));
		NiagaraCrushComponent->SetWorldLocation(SpawnNiagaraLocation);
		NiagaraCrushComponent->Activate();

		}
		if (NiagaraDamageComponent&& Shield > 0.0f)
		{
			AActor* TargetActor = GetWorld()->GetFirstPlayerController()->GetPawn();
			FVector SpawnNiagaraLocation;
			SpawnNiagaraLocation = TargetActor->GetActorLocation();
			NiagaraDamageComponent->SetWorldScale3D(FVector(0.01f, 0.01f, 0.01f));
			NiagaraDamageComponent->SetWorldLocation(SpawnNiagaraLocation);
			NiagaraDamageComponent->Activate();

		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UMyTPS_CharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UMyTPS_CharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UMyTPS_CharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UMyTPS_CharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}
